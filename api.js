const axios = require('axios');
const baseUrl = `https://selection.dorin.com/api`;
const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

const apiCall = async (url, method = 'GET') => {
  try {
    console.log(url);
    const response = await axios({
      url,
      method: 'GET',
      headers,
    });
    console.log('Status:', response.status);
    console.log('DATA:', response.data);
    return response.data;
  } catch (error) {
    console.log(error);
    return null;
  }
};

const getLanguages = () => {
  return apiCall(`${baseUrl}/languages`);
};

const getCategories = () => {
  return apiCall(`${baseUrl}/categories`);
};

const getCategory = (id) => {
  return apiCall(`${baseUrl}/category/${id}`);
};

const getTranslations = (id = 'en') => {
  return apiCall(`${baseUrl}/translations/${id}`);
};

const getVersion = () => {
  return apiCall(`${baseUrl}/version`);
};

https: module.exports = {
  getLanguages,
  getCategories,
  getCategory,
  getTranslations,
  getVersion,
};
