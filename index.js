const express = require('express');
const api = require('./api');
const PORT = 3003;
const app = express();

app.get('/', async (req, res) => {
  res.json({ ok: ':)' });
});

app.get('/version', async (req, res) => {
  const data = await api.getVersion();
  res.json(data);
});

app.get('/languages', async (req, res) => {
  const data = await api.getLanguages();
  res.json(data);
});

app.get('/categories', async (req, res) => {
  const data = await api.getCategories();
  res.json(data);
});

app.get('/category/:id', async (req, res) => {
  const { id } = req.params;
  const data = await api.getCategory(id);
  res.json(data);
});

app.get('/translations/:id', async (req, res) => {
  const { id } = req.params;
  const data = await api.getTranslations(id);
  res.json(data);
});

app.listen(PORT, () => {
  console.log(`Application running on port ${PORT}`);
});
